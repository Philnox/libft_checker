# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: wgorold <wgorold@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/04/03 20:55:37 by wgorold           #+#    #+#              #
#    Updated: 2019/04/11 13:33:13 by wgorold          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

PATH = ./

NAME=libft.a

CC=gcc

CFLAGS= -c -Wall -Wextra -Werror

SRC= ft_atoi.o ft_bzero.o ft_isalnum.o ft_isalpha.o ft_isascii.o \
	ft_isdigit.o ft_isprint.o ft_itoa.o ft_lstadd.o ft_lstdel.o \
	ft_lstdelone.o ft_lstiter.o ft_lstjoin.o ft_lstmap.o ft_lstnew.o \
	ft_lstprintstr.o ft_lstpush.o ft_lstsplit.o ft_memalloc.o ft_memccpy.o \
	ft_memchr.o ft_memcmp.o ft_memcpy.o ft_memdel.o ft_memmove.o ft_memset.o \
	ft_putchar.o ft_putchar_fd.o ft_putendl.o ft_putendl_fd.o ft_putnbr.o \
	ft_putnbr_fd.o ft_putstr.o ft_putstr_fd.o ft_strajoin.o ft_strcat.o \
	ft_strchr.o ft_strclr.o ft_strcmp.o ft_strcpy.o ft_strdel.o ft_strdup.o \
	ft_strequ.o ft_striter.o ft_striteri.o ft_strjoin.o ft_strlcat.o \
	ft_strlen.o ft_strmap.o ft_strmapi.o ft_strncat.o ft_strncmp.o \
	ft_strncpy.o ft_strnequ.o ft_strnew.o ft_strnstr.o ft_strrchr.o \
	ft_strsplit.o ft_strstr.o ft_strsub.o ft_strtrim.o ft_tolower.o ft_toupper.o

all: $(NAME) test

$(NAME): $(SRC)
	ar rc $(NAME) $(SRC)

test:
	gcc -Wall -Wextra -Werror main.c -L. -lft -I$(PATH)
ft_atoi.o:
	$(CC) $(CFLAGS) $(PATH)ft_atoi.c
ft_bzero.o:
	$(CC) $(CFLAGS) $(PATH)ft_bzero.c
ft_isalnum.o:
	$(CC) $(CFLAGS) $(PATH)ft_isalnum.c
ft_isalpha.o:
	$(CC) $(CFLAGS) $(PATH)ft_isalpha.c
ft_isascii.o:
	$(CC) $(CFLAGS) $(PATH)ft_isascii.c
ft_isdigit.o:
	$(CC) $(CFLAGS) $(PATH)ft_isdigit.c
ft_isprint.o:
	$(CC) $(CFLAGS) $(PATH)ft_isprint.c
ft_itoa.o:
	$(CC) $(CFLAGS) $(PATH)ft_itoa.c
ft_lstadd.o:
	$(CC) $(CFLAGS) $(PATH)ft_lstadd.c
ft_lstdel.o:
	$(CC) $(CFLAGS) $(PATH)ft_lstdel.c
ft_lstdelone.o:
	$(CC) $(CFLAGS) $(PATH)ft_lstdelone.c
ft_lstiter.o:
	$(CC) $(CFLAGS) $(PATH)ft_lstiter.c
ft_lstjoin.o:
	$(CC) $(CFLAGS) $(PATH)ft_lstjoin.c
ft_lstmap.o:
	$(CC) $(CFLAGS) $(PATH)ft_lstmap.c
ft_lstnew.o:
	$(CC) $(CFLAGS) $(PATH)ft_lstnew.c
ft_lstprintstr.o:
	$(CC) $(CFLAGS) $(PATH)ft_lstprintstr.c
ft_lstpush.o:
	$(CC) $(CFLAGS) $(PATH)ft_lstpush.c
ft_lstsplit.o:
	$(CC) $(CFLAGS) $(PATH)ft_lstsplit.c
ft_memalloc.o:
	$(CC) $(CFLAGS) $(PATH)ft_memalloc.c
ft_memccpy.o:
	$(CC) $(CFLAGS) $(PATH)ft_memccpy.c
ft_memchr.o:
	$(CC) $(CFLAGS) $(PATH)ft_memchr.c
ft_memcmp.o:
	$(CC) $(CFLAGS) $(PATH)ft_memcmp.c
ft_memcpy.o:
	$(CC) $(CFLAGS) $(PATH)ft_memcpy.c
ft_memdel.o:
	$(CC) $(CFLAGS) $(PATH)ft_memdel.c
ft_memmove.o:
	$(CC) $(CFLAGS) $(PATH)ft_memmove.c
ft_memset.o:
	$(CC) $(CFLAGS) $(PATH)ft_memset.c
ft_putchar.o:
	$(CC) $(CFLAGS) $(PATH)ft_putchar.c
ft_putchar_fd.o:
	$(CC) $(CFLAGS) $(PATH)ft_putchar_fd.c
ft_putendl.o:
	$(CC) $(CFLAGS) $(PATH)ft_putendl.c
ft_putendl_fd.o:
	$(CC) $(CFLAGS) $(PATH)ft_putendl_fd.c
ft_putnbr.o:
	$(CC) $(CFLAGS) $(PATH)ft_putnbr.c
ft_putnbr_fd.o:
	$(CC) $(CFLAGS) $(PATH)ft_putnbr_fd.c
ft_putstr.o:
	$(CC) $(CFLAGS) $(PATH)ft_putstr.c
ft_putstr_fd.o:
	$(CC) $(CFLAGS) $(PATH)ft_putstr_fd.c
ft_strajoin.o:
	$(CC) $(CFLAGS) $(PATH)ft_strajoin.c
ft_strcat.o:
	$(CC) $(CFLAGS) $(PATH)ft_strcat.c
ft_strchr.o:
	$(CC) $(CFLAGS) $(PATH)ft_strchr.c
ft_strclr.o:
	$(CC) $(CFLAGS) $(PATH)ft_strclr.c
ft_strcmp.o:
	$(CC) $(CFLAGS) $(PATH)ft_strcmp.c
ft_strcpy.o:
	$(CC) $(CFLAGS) $(PATH)ft_strcpy.c
ft_strdel.o:
	$(CC) $(CFLAGS) $(PATH)ft_strdel.c
ft_strdup.o:
	$(CC) $(CFLAGS) $(PATH)ft_strdup.c
ft_strequ.o:
	$(CC) $(CFLAGS) $(PATH)ft_strequ.c
ft_striter.o:
	$(CC) $(CFLAGS) $(PATH)ft_striter.c
ft_striteri.o:
	$(CC) $(CFLAGS) $(PATH)ft_striteri.c
ft_strjoin.o:
	$(CC) $(CFLAGS) $(PATH)ft_strjoin.c
ft_strlcat.o:
	$(CC) $(CFLAGS) $(PATH)ft_strlcat.c
ft_strlen.o:
	$(CC) $(CFLAGS) $(PATH)ft_strlen.c
ft_strmap.o:
	$(CC) $(CFLAGS) $(PATH)ft_strmap.c
ft_strmapi.o:
	$(CC) $(CFLAGS) $(PATH)ft_strmapi.c
ft_strncat.o:
	$(CC) $(CFLAGS) $(PATH)ft_strncat.c
ft_strncmp.o:
	$(CC) $(CFLAGS) $(PATH)ft_strncmp.c
ft_strncpy.o:
	$(CC) $(CFLAGS) $(PATH)ft_strncpy.c
ft_strnequ.o:
	$(CC) $(CFLAGS) $(PATH)ft_strnequ.c
ft_strnew.o:
	$(CC) $(CFLAGS) $(PATH)ft_strnew.c
ft_strnstr.o:
	$(CC) $(CFLAGS) $(PATH)ft_strnstr.c
ft_strrchr.o:
	$(CC) $(CFLAGS) $(PATH)ft_strrchr.c
ft_strsplit.o:
	$(CC) $(CFLAGS) $(PATH)ft_strsplit.c
ft_strstr.o:
	$(CC) $(CFLAGS) $(PATH)ft_strstr.c
ft_strsub.o:
	$(CC) $(CFLAGS) $(PATH)ft_strsub.c
ft_strtrim.o:
	$(CC) $(CFLAGS) $(PATH)ft_strtrim.c
ft_tolower.o:
	$(CC) $(CFLAGS) $(PATH)ft_tolower.c
ft_toupper.o:
	$(CC) $(CFLAGS) $(PATH)ft_toupper.c

clean:
	rm -f $(SRC)

fclean: clean
	rm -f $(NAME) a.out

re: fclean all
